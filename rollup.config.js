import fs from "fs";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import pathmodify from "rollup-plugin-pathmodify";
import { uglify } from "rollup-plugin-uglify"

export const pkg = JSON.parse(fs.readFileSync("./package.json"));
if (!pkg) {
  throw("Could not read package.json");
}
const env = process.env;
const input = "src/index.js";
const name = "app";
const globals = {};
const external = ['context']
const plugins = [
  // Resolve libs in node_modules
  resolve({
    mainFields: ['main', 'module']
  }),

  // Make sure that Mithril is included only once (if passed in INCLUDES env variable)
  pathmodify({
    aliases: [
      {
        id: "mithril/stream",
        resolveTo: "node_modules/mithril/stream/stream.js"
      },
      {
        id: "mithril",
        resolveTo: "node_modules/mithril/mithril.js"
      },
    ]
  }),

  // Convert CommonJS modules to ES6, so they can be included in a Rollup bundle
  commonjs({
    include: "node_modules/**"
  }),
]

// check if mithril is set to be external..
external.forEach(ext => {
  switch (ext) {
  case "mithril":
    globals["mithril"] = "m";
    break;
  default:
    globals[ext] = ext;
  }
});


if (env.BUILD){
  plugins.push(uglify())
}

const Config = {
  input,
  external,
  output: {
    name,
    globals,
    file: 'out.js',
    format: 'iife', 
  },
  plugins,
};

export default Config

