# Mithril ES6 Excample

This is a quick example project demonstrating the setup of a basic mithril project in es6 using Rollup with an attempt to minimize plugins, with an export to an IIFE (Immediately Invoked Function Expression) for use in a browser.


> Note, while this is an es6 example, Babel transpiling is not included in the example. If you will be using this package for production ensure you install babel or check browser compat for feature compatibility.


## Quickstart:

Clone the project, then:

```bash
npm install
npm run build OR npm run watch
```

Edit the contents within src/ to make your project. Default output goes to **out.js**, tweak the rollup.config.js to change this.
